package com.abc.webui.automation.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import com.abc.webui.automation.management.ConfigManager;
import com.abc.webui.automation.management.ILogger;
import com.abc.webui.automation.management.TestContext;
import com.abc.webui.automation.objLibrary.OR_GeicoHomePage;
import com.abc.webui.automation.utils.WebuiUtils;

public class GeicoHomePage {
	private WebDriver _driver;
	private ILogger _logger;
	protected ConfigManager _config;
	
	
	public GeicoHomePage(WebDriver driver, ILogger logger) {
		this._driver = driver;
		this._logger = logger;
		PageFactory.initElements(_driver, this);
		this._config = TestContext.Instance.config;
	}
	
	@FindBy(how = How.XPATH, using = OR_GeicoHomePage.OR_GEICO_HOME_PAGE_ACCEPT_COOKIES_BUTTON)
	private WebElement acceptCookiesButton;
	
	@FindBy(how = How.XPATH, using = OR_GeicoHomePage.OR_GEICO_HOME_PAGE_ENTER_YOUR_ZIP_TEXT_BOX)
	private WebElement enterYourZipTextBox;
	
	@FindBy(how = How.XPATH, using = OR_GeicoHomePage.OR_GEICO_HOME_PAGE_START_QUOTE_BUTTON)
	private WebElement startQuoteButton;
	
	
	public void Navigate() {
		this._driver.navigate().to(this._config.getUrl());
	}
	
	public void clickOnAcceptCookies() {
		WebuiUtils.clickElement(_driver, _logger, acceptCookiesButton, OR_GeicoHomePage.S_GEICO_HOME_PAGE_ACCEPT_COOKIES_BUTTON); 
	}
	
	public void typeZipCode(String inputZip) {
		WebuiUtils.sendKeys(_logger, enterYourZipTextBox, inputZip); 
	}
	
	public void clickStartQuote() { 
		WebuiUtils.clickElement(_driver, _logger, startQuoteButton, OR_GeicoHomePage.S_GEICO_HOME_PAGE_START_QUOTE_BUTTON);
//		return new LoginPasswordPage(this._driver, this._logger);
	}
	
//	public LoginPasswordPage ClickNext() { 
//		WebuiUtils.clickElement(_driver, _logger, startQuoteButton, OR_GeicoHomePage.S_GEICO_HOME_PAGE_START_QUOTE_BUTTON);
//		return new LoginPasswordPage(this._driver, this._logger);
//	}
	
	
}
