package com.abc.webui.automation.objLibrary;

public class OR_GeicoHomePage {
	
	public static final String OR_GEICO_HOME_PAGE_ACCEPT_COOKIES_BUTTON = "//input[@id='cookie-notice-close']";
	public static final String S_GEICO_HOME_PAGE_ACCEPT_COOKIES_BUTTON = "Accept cookies button";
	
	public static final String OR_GEICO_HOME_PAGE_ENTER_YOUR_ZIP_TEXT_BOX = "//input[@id='zip']";
	public static final String S_GEICO_HOME_PAGE_ENTER_YOUR_ZIP_TEXT_BOX = "Enter your Zip text box";
	
	public static final String OR_GEICO_HOME_PAGE_START_QUOTE_BUTTON = "//button[@class='submitButton btn btn--primary btn--full-mobile']";
	public static final String S_GEICO_HOME_PAGE_START_QUOTE_BUTTON = "Enter your Zip text box";
	
	
	
	
	
}
