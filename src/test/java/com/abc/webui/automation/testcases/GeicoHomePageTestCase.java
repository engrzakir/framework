package com.abc.webui.automation.testcases;

import org.testng.annotations.Test;

import com.abc.webui.automation.pages.GeicoHomePage;

public class GeicoHomePageTestCase extends BaseTestCase{
	private GeicoHomePage geicoHomePage;
	public GeicoHomePageTestCase() {

	}
	
	@Test(description = "Send Zip code and click on start quote button")
	public void TC1_Start_Quote_With_Zip() {
		geicoHomePage = new GeicoHomePage(this._driverHandler.driver, _log);
		geicoHomePage.Navigate();
		geicoHomePage.clickOnAcceptCookies();
		geicoHomePage.typeZipCode("22042");
		geicoHomePage.clickStartQuote();
		
//		LoginPasswordPage loginPasswordPage = _loginPage.ClickNext();
//		Assert.assertTrue(loginPasswordPage.validate());
	}
}
