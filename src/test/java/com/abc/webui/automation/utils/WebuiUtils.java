package com.abc.webui.automation.utils;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import com.abc.webui.automation.management.ILogger;

public class WebuiUtils {
	public static void sendKeys(ILogger logger, WebElement element, String inputText) {
		element.clear();
		element.sendKeys(inputText);
	}
	
	public static void clickElement(WebDriver driver, ILogger logger, WebElement button, String buttonName) {
		WaitUtils.waitForElement(driver, button);
		button.click();
	}
}
